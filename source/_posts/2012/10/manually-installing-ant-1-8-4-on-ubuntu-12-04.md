---
title: Manually installing Ant 1.8.4 on Ubuntu 12.04
date: 2012-10-02 18:06:24
tags:
  - devops
  - bash 
  - ubuntu
  - ant
---

{% asset_img hor.png "Happy Hor" %}

My **handy step guide** for installing Ant.

<!-- more -->

## Step1: Download a fresh copy

```
$> wget http://apache.heikorichter.name/ant/binaries/apache-ant-1.8.4-bin.tar.gz
```

## Step 2: Extract distribution archive

Extract the distribution archive `apache-ant-1.8.4-bin.tar.gz` to a more appropriate directory, in my case `/usr/local`

```
$> tar xvfz apache-ant-1.8.4-bin.tar.gz
$> sudo mv apache-ant-1.8.4 /usr/local
```

## Step3: Setup Ant Home

Edit file `~/.profile` and add following lines:

```bash
ANT_HOME="/usr/local/apache-ant-1.8.4"
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:$ANT_HOME/bin"
```

Remember load your profile changes `source ~/.profile`

```
$> source ~/.profile
```

## Step4: Verify installation

Now run `ant -version` in a terminal and you will get something like following

```
$> ant -version
Apache Ant(TM) version 1.8.4 compiled on May 22 2012
```

Have fun!
---
title: 'And yes, another guide to setup your server with Public-Key authentication'
date: 2012-06-22 22:23:39
tags:
---

{% asset_img facepalm.jpg "Oh, not again!" %}

Indeed, I want to create another new guide for **how to access a remote server using Public-Key Authentication**. Why? Basically: *I want to use my blog as a technology notebook for my own*.

<!-- more -->

So, Let's ask, Do you want to connect to your remote server through ssh without having to authenticate every time?. If the answer is yes,  follow these steps:

**Notes**

* This setup is based on Ubuntu 11.10
* For this demo username is  **le-user**  and server name **le-server.com**


## Step 1
Create a public/private key for your hosting server in your local computer.  In a terminal go to you **<code>.ssh</code>** folder and execute **<code>ssh-keygen</code>** .

* For the question *Enter file in which to save the key*,  enter this name **le-server.com.key** or whatever name you like.
* For questions related to passphrase, leave it in blank, only hit enter.


```
luis@myblog:~$ cd .ssh
luis@myblog:~/.ssh$ ssh-keygen 

Generating public/private rsa key pair.
Enter file in which to save the key (/home/luis/.ssh/id_rsa): le-server.com.key
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in le-server.com.key.
Your public key has been saved in le-server.com.key.pub.
The key fingerprint is:
98:87:7b:33:a3:08:77:10:80:88:63:fe:94:c2:55:e8 luis@myblog
The key's randomart image is:

+--[ RSA 2048]----+
|+.  ..           |
|o. ..            |
| +o.             |
|+ oE.  +         |
| +.o  + S        |
|  +. . o         |
|   .o o . o      |
|     o o + .     |
|      . o        |
+-----------------+
```

## Step 2
Copy generated key to your hosting server.  Execute **<code>ssh-copy-id -i le-server.com.key le-user@le-server.com</code>**. (It will prompt you to enter your hosting user password)

```
luis@myblog:~/.ssh$ ssh-copy-id -i le-server.com.key le-user@le-server.com

The authenticity of host 'le-server.com (10.10.10.10)' can't be established.
DSA key fingerprint is 34:47:0f:e9:1a:33:eb:33:eb:cc:33:59:3a:02:80:b6.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'le-server.com ,10.10.10.10' (DSA) to the list of known hosts.
le-user@le-server.com's password: 

Now try logging into the machine, with "ssh 'le-user@le-server.com'", and check in:

  ~/.ssh/authorized_keys

to make sure we haven't added extra keys that you weren't expecting.
```

## Step 3
In your computer, in the folder .ssh open or create a filename called **config** and add the following peace of code at the end of the file

```
Host le-server.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/le-server.com.key
User le-user
```


## Step 4
Execute **<code>ssh-add ~/.ssh/le-server.com.key</code>**. This will add the private key to the SSH agent so that you will  be prompted for the passphrase only once.

## Step 5
From your local computer. Open a terminal and just type <code>ssh le-server.com</code>  and **voila!**

```
luis@myblog:~$ ssh le-server.com

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.

(uiserver):le-user:~ >
```

Happy coding :D
---
title: 'fatal: CRLF would be replaced by LF'
date: 2012-05-28 17:28:11
tags:
  - git
  - dos2unix
  - quickfix
---

{% asset_img firstcommit.png "Commit of hell" %}

I remember it was a Friday, I wanted to commit my changes and go home to start a good weekend. 

When I tried to add new files to my local git repository, I experienced this issue:

``` bash
$> git add .
fatal: CRLF would be replaced by LF in /core/sites/all/modules/thirdlibrary/file.xyz
```

<!-- more -->

After researching a bit, I discovered that the problem was being caused by a third library included in my project and for some reason Git didn't want to include those files in my repository.

According to the inexhaustible knowledge of StackOverflow, [here](http://stackoverflow.com/questions/2825428/why-should-i-use-core-autocrlf-true-in-git) and
also [here](http://stackoverflow.com/questions/7068179/convert-line-endlings-for-whole-directory-tree-git) explain the problem and how to solve it.

Commonly this issue is produced by how the end of line is treated on different operative systems. If you ever encounter this problem, I suggest you do the following:

1.- **Install dos2unix**. It's a DOS/MAC to UNIX and vice versa text file format converter.

``` bash
$> sudo apt-get install dos2unix
```

2.- **Check your files with dos2unix tool**. Make sure that dos2unix checks all files on your conflictive folder.
Those files who are ok, dos2unix will not change it.

``` bash
$> find  /core/sites/all/modules -type f -exec dos2unix {} +
```

The problem will be gone the next time you try to add your files to your git repository.

---
title: Node 8.4 in your Pi 3 (Revisited)
date: 2017-09-06 00:27:42
tags:
 - devops
 - docker
 - rpi
 - node
---

{% asset_img itsallinthehips.jpg "It is all in the swap" %}

I've finally managed to compile the [Michael Hart Alpine](https://github.com/mhart/alpine-node) version in my Raspberry Pi 3. I tried to have this image ready for my article [Dockerizing Node 8 on Raspberry Pi](https://toub.es/2017/07/24/dockerizing-node-8-on-raspberry-pi/) but it was not possible. At the end, the key was to give a little more swap memory to the system, as is obvious Raspbian has few resources.

The way to add 512MB of swap memory from your SdCard would be something like this

```
sudo dd if=/dev/zero of=/tmp/swapfile bs=1M count=512 && \
sudo mkswap /tmp/swapfile && \
sudo swapon /tmp/swapfile
```

With this trick, the compilation was a success.

Having said that, I updated my version of Node Alpine for Raspberry Pi 3 [kafebob/rpi-alpine-node](https://hub.docker.com/r/kafebob/rpi-alpine-node/) and you will get **Node 8.4.0, Npm 5.3.0 and Yarn 0.27.5** ready to use.

<!-- more -->

In case you want to compile the image by yourself, you can get a Dockerfile copy [here](https://github.com/xcafebabe/gobstoppers/blob/master/node/Dockerfile).

And if you trust me, bellow I show you the way to pull a copy and start to use it:

```
sudo docker run -it --rm kafebob/rpi-alpine-node
```

If you are interested in the NVM-based version of Node, I have created a new image that offers **Node 8.4.0** and **Npm 5.3.0** using [Node Version Manager](https://github.com/creationix/nvm), to get and execute this image just

```
sudo docker run -it --rm kafebob/rpi-alpine-nvm
```

If you want to compile your own version you can find the Dockerfile in my repository [here](https://github.com/xcafebabe/gobstoppers/blob/master/nvm/Dockerfile).

I hope this information will be useful to someone.

Happy Coding.
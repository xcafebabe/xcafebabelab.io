---
title: High quality GIF with FFmpeg and Docker
tags:
  - docker
  - gif
  - devops
  - utils
  - ffmpeg
date: 2017-09-11 14:26:41
---

## Intro

{% asset_img "first.gif" "First gif on internet" %}

For a few days I have been looking for alternatives **to transform short screencasts into GIF images** in order to show you some ideas on my blog. You know, an image is worth a thousand words.

I had the opportunity to play with these tools:

- [Asciinema](https://asciinema.org)
- [Peek](https://github.com/phw/peek)
- [Giphy](https://giphy.com/)

[Asciinema](https://asciinema.org) **I love it!**, IMHO is the best way to record a command line session and share it with colleagues. Actually, I used it in my article [Self-signed Https support](https://toub.es/2017/08/27/self-signed-https-support/) and I will continue using it. In the other hand, you can't record anything other than a terminal session but it's the service they offer and it works great.

[Peek](https://github.com/phw/peek) is also great, but at the time of writing this article, in my Arch Linux didn't work properly.

[Giphy](https://giphy.com/) is amazing but I ruled it out because I don't want my gifs to be hosted in a datacenter in the middle of nowhere.

<!-- more -->

## Gif resources

In [Github.com](https://github.com) you can find a repository with a curated list of awesome GIF resources [here](https://github.com/davisonio/awesome-gif) and I was interested in [this technique](https://github.com/davisonio/awesome-gif#high-quality-gif) based on [FFmpeg](http://ffmpeg.org/), turns out **ffmpeg** can now output **high quality GIF**.

The drawback with this technique is that you have to write __magic spells__ in the command line to get desired results. If you are interested to read details about these magic spells, I recommend you to read [High quality GIF with FFmpeg](http://blog.pkh.me/p/21-high-quality-gif-with-ffmpeg.html).

{% asset_img potion.jpg "Potions everywhere" %}


## kafebob/video2gif

So I thought, it would be nice to create a Docker image that encapsulates all the complexity of this process and allows me to get the desired result without having to install [FFmpeg](http://ffmpeg.org/) or having to remember commands and complex parameters.

And that's what I did, a container called [kafebob/video2gif](https://hub.docker.com/r/kafebob/video2gif/) based on Linux Alpine with ffmpeg installed and all the necessary adjustments for a proper conversion from video to gif. This container is for x64 architecture, if anyone needs it for Raspberry Pi please let me know in the comments.

The container weighs 30MB and the Dockerfile of this container can be found [here](https://github.com/xcafebabe/gobstoppers/blob/master/video2gif/Dockerfile).

<a href="https://microbadger.com/images/kafebob/video2gif" title="Get your own image badge on microbadger.com" class="nofancy"><img src="https://images.microbadger.com/badges/image/kafebob/video2gif.svg"></a>


### Install

In order to transform your videos into gifs run the container like this

```
sudo docker run --rm -v=`pwd`:/tmp/ffmpeg kafebob/video2gif video.mp4 video.gif
```

Also to make your life easier, just create an alias in your profile (`.bashrc`)

```
alias video2gif='sudo docker run -v=`pwd`:/tmp/ffmpeg kafebob/video2gif'
```

And then just call the transformation like this

```
vide2gif video.mp4 video.gif
```

Remember that you must have [Docker](https://docs.docker.com/engine/installation/) installed and running.


### How to use it

Assuming your video is called `video.wmv` and you want to transform it into a gif called `video.gif`, you will have to run

```
video2gif video.wmv video.gif
```

After a few seconds you will get the desired gif.

The script also accept these options

- `-f <framerate>` 10 fps is the default and works well, but in many cases you can even get good results with lower framerates.
- `-y <yscale>` 480 px wide is the default and automatically determine the height while preserving the aspect ratio.
- `-s <skip_sec>` 0 is default and will skip the first N seconds of video.
- `-t <duration_sec>` If specified creates a N second output.

From previous example if we do

```
video2gif -f 30 -y 320 -s 10 -t 5 video.wmv video.gif
```

It will generate a gif with the next specifications
- 30 frames per second.
- 320 pixels wide preserving the aspect ratio.
- Starts at second 10.
- 5 seconds long.

### Practical example

Using as a reference [Oh my god](https://www.youtube.com/watch?v=Jc0r5ilDiXw) video. [Link](/2017/09/11/high-quality-gif-with-ffmpeg-and-docker/oh-my-god.mp4) to download video.

I'm going to create one gif using 10 fps, 480px width size, starting at 13 second and 3 seconds long.

```
video2gif -s 13 -t 3 ohmygod.mp4 ohmygod.gif
```

And this is the result, a decent GIF of 3.4MB.

{% asset_img output2.gif "Omaigaa!" %}

I hope it will be useful

Happy coding!

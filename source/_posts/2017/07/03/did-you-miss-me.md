---
title: Did you miss me?
date: 2017-07-03 10:20:41
tags:
  - devops
  - docker
  - hexo
---

{% asset_img featured.png "Meao!" %}

These days I've been thinking about **closing my blog** (I have years without writing) but because **I love** to **share experiences** of my battles against the technology and because  **I love** to **provide solutions** so others could find an easier path to move on I've decide to  give me a new opportunity and reboot my mental discipline and **start again** (never is too late).

<!-- more -->

Nowadays my best technological universes preferences are [Docker](https://www.docker.com/), [NodeJs](https://nodejs.org/), [React](https://facebook.github.io/react/), [Redux](http://redux.js.org/), [React Native](https://facebook.github.io/react-native/), [Cordova](https://cordova.apache.org/), [Angular](https://angularjs.org/), [Drupal](https://www.drupal.org/) and [Raspberry Pi](https://www.raspberrypi.org/), I will be writing about these little things and I hope would be useful to somebody out there.

By the way the content and source code of this blog is completely **free** and **open** and recently I decided to migrate from [Drupal 7](https://www.drupal.org/drupal-7.0) to [Hexo](https://hexo.io/) a static Html generator based on Node.js, you can **pull a copy** of my repository [here](https://gitlab.com/xcafebabe/xcafebabe.gitlab.io) and **start your own version**.

{% asset_img hexo-blog.png "Hexo Kudos"%}

In README file you will find how to run locally this blog and start to create your own blog version. Basically you will need `docker` and `docker-compose` and execute following commands:

```
git clone https://gitlab.com/xcafebabe/xcafebabe.gitlab.io.git \
  && cd xcafebabe.gitlab.io \
  && cp env-example .env \
  && sudo docker-compose up
```

Wait until you read on the console `Hexo is running at http://localhost:4000/. Press Ctrl+C to stop.` after that follow the link.

Happy coding!

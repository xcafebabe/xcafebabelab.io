---
title: Your own Agario server on Raspberry Pi
tags:
  - docker
  - rpi
  - games
  - devops
date: 2017-08-03 00:08:49
---



{% asset_img screenshot.png "You haven't played yet?" %}

If you don't know what [Docker](https://www.docker.com/docker-windows) is I recommend you to read first [Docker & Raspberry Pi, perfect combo!](https://toub.es/2017/07/13/docker-raspberry-pi-perfect-combo/), in there you will find briefly what it is and how to install it in your Raspberry Pi.

In short, Docker will allow you **to create in one minute** an Agario server on your Pi:

```
sudo docker run -it --name my-private-agario -p 3000:3000 kafebob/rpi-agario
```

<!-- more -->

When you read in console the output `[DEBUG] Listening on 0.0.0.0:3000` you are going to be able to test Agario game in your browser.

<pre>
[10:11:19] Finished 'build-client' after 14 s
[10:11:19] Starting 'build'...
[10:11:19] Finished 'build' after 307 μs
[10:11:19] Starting 'run'...
[10:11:19] Finished 'run' after 187 ms
[10:11:19] [nodemon] 1.11.0
[10:11:19] [nodemon] to restart at any time, enter `rs`
[10:11:19] [nodemon] watching: *.*
[10:11:19] [nodemon] starting `node ./server/server.js config.json`
<strong>[DEBUG] Listening on 0.0.0.0:3000</strong>
</pre>

Using the IP address of your Pi, type in your browser  http://YOUR_PI_ADDRESS:3000

{% asset_img agario.png "Ready to play!" %}

That's it!. Enjoy the game  with your friends!

When you are done, just finish the game hitting `Ctrl+C` or in a console type `sudo docker stop my-private-agario`

If you want to start again the game just `sudo docker start my-private-agario`

## Behind scenes

I take also the chance with this article to describe what is behind the image `kafebob/rpi-agario`. Below is the `Dockerfile` that creates the previous container.

```docker
FROM kafebob/rpi-alpine-node
MAINTAINER Luis Toubes <luis@toub.es>

RUN apk add --update --no-cache git && \
    git clone https://github.com/huytd/agar.io-clone.git agario && \
    source $PROFILE &&  cd agario && \
    npm install && npm cache clean --force

WORKDIR /agario

EXPOSE 3000

CMD [ "/bin/bash", "-c","source $PROFILE && npm start"]
```

Thanks to Docker technology you can extend your Docker image from an existing one and be able to provide more specialized services.  Have a look at **line 1** `FROM kafebob/rpi-alpine-node:8.2.1`, the docker image `kafebob/rpi-alpine-node`  is an Alpine Linux 3.6 with Node 8.2.1 ready to use. I describe a bit about  `kafebob/rpi-alpine-node` in [Dockerizing Node 8 on Raspberry Pi](https://toub.es/2017/07/24/dockerizing-node-8-on-raspberry-pi/).

`kafebob/rpi-alpine-node` uses [NVM](https://github.com/creationix/nvm) to manage Node versions and is only available through user `root  `.

From **lines 3 to 6** I clone source code from Agario repository and follow the default Node.js workflow to install and execute a node app.

I'm not completely happy about `kafebob/rpi-alpine-node` because you need to load NVM every time you use a `RUN` or `CMD`. I'm working on a fork from this [alpine-node repository](https://github.com/mhart/alpine-node) to compile Node on your Pi without NVM dependency and to reduce size of the image. Hopefully in some days I will write about it.

This [Agario server](https://github.com/huytd/agar.io-clone) is an open source project and is not the real one BUT! is a very good clone. I used it as a reference to prove how much you can get from Docker. Once you are done with this container game you just need to delete the container and image and your Pi is going to be clean from any Agario dependency.

To delete image and container

```bash
echo "Deleting container ..."
sudo docker rm my-private-agario
echo "Deleting image ..."
sudo docker image rm kafebob/rpi-agario
```

Happy coding!
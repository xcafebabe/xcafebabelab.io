---
title: Hosting browser games on your Pi
tags:
  - docker
  - nginx
  - rpi
  - devops
date: 2017-08-12 22:35:28
---


{% asset_img "spongebob.jpg" "You don't need a license to drive Nginx" %}

I have prepared another Docker container for the Raspberry Pi 3, this time it's a **Nginx 1.13.1** with **Alpine 3.6** ready to be used.

I have used as reference the official [Dockerfile](https://github.com/nginxinc/docker-nginx/blob/master/mainline/alpine/Dockerfile) from Nginx and this [Dockerfile](https://hub.docker.com/r/smebberson/alpine-nginx/) from [@smebberson](https://twitter.com/smebberson), you can see the final result in my repository [here](https://github.com/xcafebabe/gobstoppers/tree/master/nginx), the size of this image is  25MB :), also you can get a copy as usual

```
sudo docker run --rm -it --name nginx-server -p 80:80 kafebob/rpi-alpine-nginx
```

<!-- more -->

With this image I have learned to use another of the Docker killers features, **data volumes**.

Data volumes are designed to persist data, independent of the container’s lifecycle. Docker therefore never automatically deletes volumes when you remove a container. You can add a data volume to a container using the `-v` flag when you run a container. You could also use the `-v` multiple times to mount multiple data volumes.

This means that we can have information outside the container and make it available using appropriate settings. As a software engineer this is fantastic, since you can have a whole development environment ready in a matter of minutes without having to install or to configure anything on your host.

# Demo

As an example, see how straightforward it would be to use this container to serve browser games.

## Step 1. Choose your game(s)

In [github.com](https://github.com) you can find an awesome [list](https://gist.github.com/roachhd/d579b58148d7e36a6b72#user-content-browser-based) with open source games for the browser.

I'm going to use [Survivor game](https://github.com/scottschiller/SURVIVOR) and I'll save it in the folder `games`.

```
mkdir ~/games && cd $_ && \
git clone https://github.com/scottschiller/SURVIVOR.git
```

## Step 2. Load Nginx Container

Once you have downloaded all the games you want, it's time to start a Nginx container just like this

```
sudo docker run --rm --name browser-games-box -v ~/games:/var/www -p 80:80 kafebob/rpi-alpine-nginx
```

Notice flag `-v ~/games:/var/www` which tells docker that folder `~/games` that we have recently created will point
to `/var/www` folder inside the container.

## Step 3. Have fun and play

Open a browser and type the IP of your Pi. In my case, the IP is 192.168.1.135 so then url is http://192.168.1.135

If you don't know the IP of your Raspberry, in the article [Docker & Raspberry Pi, perfect combo!](https://toub.es/2017/07/13/docker-raspberry-pi-perfect-combo/) is explained [here](https://toub.es/2017/07/13/docker-raspberry-pi-perfect-combo/#First-time-boot) how you could find the ip.

And it's done, you will see in your browser a folder list with the games you've copied in folder `~/games`. You can add more games if you like, the Nginx container will see all the changes immediately.

{% asset_img "screen1.png" "List of games" %}
{% asset_img "screen2.png" "Survivor" %}

# Nginx Image Extra

This Nginx image exposes also the folder `/etc/nginx/conf.d` as a data volume, which means you can provide your custom server configuration (enable ssl, change error page location, etc.). The following command example will create a container and will mount as well nginx configuration folder.

```
sudo docker run --rm --name browser-games-box -v ~/games:/var/www \
-v ~/custom-nginx-conf:/etc/nginx/conf.d -p 80:80 kafebob/rpi-alpine-nginx
```

In `~/custom-nginx-conf` folder save your custom nginx server configuration.

Any question?, leave your comments below.

Happy coding!

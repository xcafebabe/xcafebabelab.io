---
title: Self-signed Https support
tags:
  - docker
  - devops
  - ssl
  - nginx
date: 2017-08-27 14:40:50
---


{% asset_img spongebob-police.jpg "You know what's funnier than Http? Https!" %}

In [Hosting browser games on your Pi](https://toub.es/2017/08/12/hosting-browser-games-on-your-pi/) article I show you how to easily create a web container on your Raspberry Pi to host your web content.

I have updated `kafebob/rpi-alpine-nginx` image **to programmatically enable SSL** in your web container. In other words, the container is able to create self-signed SSL certificates and is also able to use your own SSL certificates in a simple way.

Docker allows you to pass environment variables and using them to change the behavior of docker containers.

If you pass the environment variable `$NGINX_SERVERNAME` when you create a container from `kafebob/rpi-alpine-nginx` then SSL is going to be enabled. `$NGINX_SERVERNAME` sets name of the virtual server and will be used to generate SSL self-signed certificates.

<!-- more -->

## Self-signed SSL Certificates

Assuming your Raspberry Pi is called `gaspar` and this Pi unit can be founded on your internal network using `ping gaspar.local` you are able to add Https support to the game container recently created in [Hosting browser games on your Pi](https://toub.es/2017/08/12/hosting-browser-games-on-your-pi/).

For this support what you would have to do is:

```bash
sudo docker run -e NGINX_SERVERNAME=gaspar.local \
                --rm \
                --name browser-games \
                -v ~/games:/var/www \
                -p 80:80 -p 443:443 \
                kafebob/rpi-alpine-nginx
```

### Command explained

* `-e NGINX_SERVERNAME=gaspar.local` - Environment variable with the name of your virtual server.
* `--rm` - Automatically remove the container when it exits.
* `--name browser-games` - __browser-games__ is the name for this container.
* `-v ~/games:/var/www` - Your folder `~/games` on your Pi is mapped with default Nginx root folder `/var/www` on this container.
* `-p 80:80` - Linked port 80 on host to port 80 on container.
* `-p 443:443` - Linked port 443 on host to port 443 on container.

You can see a quick-demo below

<script type="text/javascript" src="https://asciinema.org/a/ja1wNnHgJc0wi3nEBiFm5vzbq.js" id="asciicast-ja1wNnHgJc0wi3nEBiFm5vzbq" data-speed="1.5" async></script>


## Custom SSL Certificates

If you want to use your own SSL certificates, you must map a folder in your Pi with container folder `/etc/nginx/certs`. In this folder must exist two files, the certificate file (`.pem` extension) and private key file (`.key` extension), the name of these files must be equal to `$NGINX_SERVERNAME` environment variable name (image `kafebob/rpi-alpine-nginx` is prepared to work with this naming convention).

For instance, if `$NGINX_SERVERNAME` is equal to `gaspar.local`, certificate should be called `gaspar.local.pem` and private key `gaspar.local.key`.

```
sudo docker run -e NGINX_SERVERNAME=gaspar.local \
                --rm \
                --name browser-games \
                -v ~/games:/var/www \
                -v ~/browser-certificates:/etc/nginx/certs \
                -p 80:80 -p 443:443 \
                kafebob/rpi-alpine-nginx
```

In the next clip you will see how I manage to setup a SSL Certificate previous generated in my host environment.

<script type="text/javascript" src="https://asciinema.org/a/tPmq0VlXhxGE2n4qhRoiMbIld.js" id="asciicast-tPmq0VlXhxGE2n4qhRoiMbIld" data-speed="1.5" async></script>

## Extra: Diffie Hellman Key-exchange

The image `kafebob/rpi-alpine-nginx` allows you to activate [Diffie-Hellman (DH) key-exchange](https://www.khanacademy.org/computing/computer-science/cryptography/modern-crypt/v/diffie-hellman-key-exchange-part-2). For demo purposes DH key-exchange is not needed but is always nice to have it ready to use.

To enable this feature in your Nginx server you need to pass to the container an environment variabled called `NGINX_DIFFIE_HELLMAN` with value `on` (by default it's disabled). If this variable has `on` value and the container does not detect `dhparams.pem` inside folder `/etc/nginx/certs` it will proceed to generate a 2048-bit long Diffie-Hellman Params File (please take a 20 minute break). You can use your own `dhparams.pem` and bind it to container folder `/etc/nginx/certs`.

Any thoughts? I hope you find it useful!

Happy coding!

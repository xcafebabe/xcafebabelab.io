---
title: Are you still paying for SSL Certificates?
tags:
  - gitlab
  - hosting
  - https
  - free
date: 2017-10-25 21:41:47
---


Just imagine: you have your whole life using [1and1](https://www.1and1.com/) as your Hosting provider and one of your hosted services is your blog, a [Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/) blog-project in which you want to invest some of your free time to return a little bit back to the community.

Now imagine that one day in 2017 you wake up and say: __I'll add Https support to my blog, the blog doesn't need it but it would be nice to have it__ and at that moment you realize about the Hosting provider's goose that lays golden eggs, SSL Certificates.

Try to imagine my face when I discovered that [1and1](https://www.1and1.com/) doesn't allow you to use [Let's Encrypt ssl certificates](https://letsencrypt.org/), a free, automated and open certificate authority.

{% asset_img ssl-surprise.jpg "SSL, the goose that lays golden eggs" %}

I've been thinking about some alternatives to take advantage of Let's Encrypt free certificates and my friend [@ganlub](https://twitter.com/ganlub) recommended me to try [GitLab](https://about.gitlab.com/), one of their services is **[GitLab Pages](https://about.gitlab.com/2016/04/04/gitlab-pages-get-started/)**, a service that can be used **to host static websites for free** using **custom domains** and **Let's Encrypt certificates**.

<!-- more -->

Currently is open in GitHub this [Feature Request](https://github.com/isaacs/github/issues/156), developers are requesting Https support using custom domains for GitHub Pages, however, until the time of writing this article this feature request has not been fulfilled.

Because of this reason I decided to try [GitLab.com](https://gitlab.com/) and so far I have no regrets at all!. Lately I'm using **GitLab services** more frequently and to be honest the amount of **features are higher** than those currently offered by **GitHub services**.

I've written this introduction to justify the reason **to migrate my blog from 1and1 to GitLab** and what I'm going to do next is to describe the setup to host my blog in GitLab with Https support using the custom domain [toub.es](https://toub.es).

This setup is divided in 4 important topics:

1. **Content hosting**. As a starting point you could find my [blog repository](https://gitlab.com/xcafebabe/xcafebabe.gitlab.io) interesting, also as a suggestion you should read about [GitLab rules and good practices](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/) to host your content using GitLab.
2. **Domain Management**. I'm still using [1and1](http://1and1.es) as my Domain provider.
3. **Let's encrypt SSL Management**. It's a manual process that would no take more than 5 minutes to complete.
4. **Profit**. Enjoy close to the technological beast.

**Advantages**: Operational costs are low and high availability is guaranteed, thanks to [GitLab.com](https://gitlab.com/) and [Let's Encrypt.org](https://letsencrypt.org/).

**Disadvantages**: Approximately every 90 days you will have to renew your SSL certificate, although it's something  that will take you 5 minutes. (Don't worry Let's Encrypt will let you know when is time to renew your SSL).

If you've read this far and you are still interested in the topic, congratulations it's time to move on to the next chapter of this article, [Publish and secure your blog for free using GitLab Pages](http://toub.es/2017/10/25/publish-and-secure-your-blog-for-free-using-gitlab-pages/), in there you'll find out how to do this.

Thank you for your reading and also Thank you GitLab for so much love.

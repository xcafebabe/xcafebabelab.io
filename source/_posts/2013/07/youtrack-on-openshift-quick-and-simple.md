---
title: 'YouTrack on OpenShift, quick and simple'
date: 2013-07-15 18:57:02
tags: 
  - devops
  - linux
  - youtrack
  - openshift
---

{% asset_img youtrack-openshift.png YourTrack and Openshift %}

I've been looking for a cheap (ok, free!) way to manage a queue of tasks/issues because lately I have too many. The solution must be flexible enough to manage the tasks on any device and any place.

[JetBrains](http://www.jetbrains.com/) offers a free license for its wonderful issue tracker called [YouTrack](http://www.jetbrains.com/youtrack/), the responsive web design of YouTrack is enough to manage tasks from any device. Furthermore, [RedHat](http://www.redhat.com/) and its fabulous product [OpenShift](https://www.openshift.com/) offers free hosting for almost all main technologies, this solution is enough to manage tasks from anywhere (ok, anywhere with an internet connection). YouTrack is based on Java and OpenShift support Java, perfect solution was found.

<!-- more -->

Through the following steps, I'll show how to deploy **YouTrack 4.2.4** on **OpenShift**, let's start.

## Step 1: Do you have an OpenShift account?

If you don't, run and open an account [here](https://openshift.redhat.com/app/account/new).

## Step 2: Install & Setup rhc

rhc is the command-line tool for OpenShift. You will need to install also Git and Ruby. Everything is well explaining in 
the  [official installation guide](https://www.openshift.com/developers/rhc-client-tools-install).

After setup, open the command line and type <code>rhc account</code>. You will see something similar to this

```
$ rhc account
Your authorization token has expired. Please sign in now to continue.
Password: ********

Server:                     openshift.redhat.com
Login:                      luis@toub.es
Plan:                       Free
Gears Used:                 0
Gears Allowed:              3
Allowed Gear Sizes:         small
SSL Certificates Supported: no
```


## Step 3: Create a domain for YouTrack

Each domain has a namespace value that will be used as part of the public URL for an application. Is mandatory that you create a namespace.
For example, when creating a domain with the namespace "bytesauce", any application created in that domain will have the public URL:
http://{appname}-bytesauce.rhcloud.com

```bash
$ rhc domain-create bytesauce
```

After namespace creation you need to create an OpenShift application to serve web requests. For YouTrack we will use a Tomcat7 instance.
When your application is created, a domain name that is a combination of the name of your app and the namespace of your domain will be registered in OpenShift DNS. For example, if the name of you OpenShift application is *todos*, your public url for YouTrack will be, http://todos-bytesauce.rhcloud.com


```
$ rhc create-app todos jbossews-2.0
Application Options
-------------------
  Namespace:  bytesauce
  Cartridges: jbossews-2.0
  Gear Size:  default
  Scaling:    no

Creating application 'todos' ... done

  Application todos was created.
  The cartridge jbossews deployed a template application

Waiting for your DNS name to be available ... done

Cloning into 'todos'...
Warning: Permanently added the RSA host key for IP address '23.20.144.210' to the list of known hosts.

Your application 'todos' is now available.

  URL:        http://todos-bytesauce.rhcloud.com/
  SSH to:     SECRETKEY@todos-bytesauce.rhcloud.com
  Git remote: ssh://SECRETKEY@todos-bytesauce.rhcloud.com/~/git/todos.git/
  Cloned to:  /home/luis/Labs/work/todos

Run 'rhc show-app todos' for more details about your app.
```

<code>rhc create-app todos jbossews-2.0</code> command created your application space remote and locally.

## Step 4: Download YouTrack

You will notice that your local application space has this folder structure

```
$ ls /home/luis/Labs/work/todos -la
total 32
drwxr-xr-x 6 luis luis 4096 Jul 15 15:57 .
drwxrwxr-x 5 luis luis 4096 Jul 15 15:57 ..
drwxrwxr-x 8 luis luis 4096 Jul 15 15:57 .git
drwxrwxr-x 6 luis luis 4096 Jul 15 15:57 .openshift
-rwxrwxr-x 1 luis luis 2136 Jul 15 15:57 pom.xml
-rw-rw-r-- 1 luis luis  179 Jul 15 15:57 README.md
drwxrwxr-x 3 luis luis 4096 Jul 15 15:57 src
drwxrwxr-x 2 luis luis 4096 Jul 15 15:57 webapps
```

The application space is a git project where you can easily push your changes to the remote application space.
Copy last version of YouTrack war into <code>webapps</code> folder of your local application space, in this example webspace folder is located
here <code>/home/luis/Labs/work/todos/webapps</code>

```
$ cd /home/luis/Labs/work/todos/webapps
$ wget http://download-ln.jetbrains.com/charisma/youtrack-4.2.4.war
```


## Step 5: Configure YouTrack

There’s one little problem we need to fix before pushing YouTrack to the server. YouTrack comes with a built in database which is by default located in {user.home} and OpenShift team prevent that you write on this folder. But, they are not bad people and offer you a persistent directory for your data. So, you will need to change a couple of properties in web.xml of your YouTrack war.

1. Open <code>youtrack-4.2.4.war</code> with your default zip program
2. From inside the archive, open <code>WEB-INF/web.xml</code>
3. Replace <code>${user.home}/teamsysdata</code> by <code>${user.home}/app-root/data/teamsysdata</code> and <code>${user.home}/teamsysdata-backup</code> by <code>${user.home}/app-root/data/teamsysdata-backup</code>
4. Save the file (confirm to update the .war archive)

## Step 6: Deploy YouTrack

To deploy YouTrack as the root of the domain, the war file must be called ROOT.war and you need to remove some files and folders from your local application space.

```
$ cd /home/luis/Labs/work/todos
$ mv webapps/youtrack-4.2.4.war webapps/ROOT.war
$ git rm -r pom.xml src/
```

Now YouTrack is ready to deploy

```
$ git add .
$ git commit -m "Deploying YouTrack 4.2.4"
$ git push
```

## Step 7: Ready to use

When upload is finished an action hook is triggered to start the deployment process. It will take a while (around 3-4 minutes) until YouTrack is ready to use, maybe the first time you try to access through the public url you will get a Proxy Access Error, try to reload once more if this happens and then you will see YouTrack configuration page.

Enjoy!!!!

**EDIT: 16.July.2013** - Official features in YouTrack said *Full Issue View is optimized specifically for mobile platform*, but until now I could not find the way to comfortably read task on my android Mobile. Decided to use an android app called [Okie Dokie](https://play.google.com/store/apps/details?id=com.strintec.youdroid) for managing YouTrack tasks, fully recommended!
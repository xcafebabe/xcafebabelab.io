---
title: About
date: 2017-06-23 17:25:46
comments: false 
---

![Luis Toubes](/assets/r2d2.jpg)

Hola!, my name is Luis Toubes, a **Full-Stack Web Developer** with **inexhaustible passion** about **web technologies**.

This site is a place for me to show my work and let people know what I can do. 

In a nutshell:

- Venezuelan expat living and working in Barcelona.
- Interested about every stage of the product life cycle.
- Building websites and product experiences for 10+ years.
- My preferred software stack include: JavaScript, ReactJS, NodeJS, Redis, MongoDB and Docker.
- Comfortable also with Drupal, AngularJS, Ionic Framework and Apache Cordova development stacks.

Besides enjoying **programming**, I love **traveling** and **cooking**. Find out what I'm up to now, connect with me on [LinkedIn](https://lnked.in/toub) or say Hola! at <luis@toub.es>. You can also read my open projects on [Github](https://github.com/xcafebabe?tab=repositories) and [Gitlab](https://gitlab.com/users/xcafebabe/projects).

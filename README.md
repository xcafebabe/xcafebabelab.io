# Toub.es Hexo version

[![build status](https://gitlab.com/xcafebabe/xcafebabe.gitlab.io/badges/master/build.svg)](https://gitlab.com/xcafebabe/xcafebabe.gitlab.io/commits/master)

[toub.es](https://toub.es) website based on [Hexo](https://hexo.io/). This repo is hosting source code and production files.

## Getting Started

To run this website in your local environment the only requirement is to have `docker` and `docker-compose` installed.

To use this source code as a starting point for your personal blog remember to change properties like `title`, `author`, `url` in `_config.yml`.

[Theme Next](https://github.com/iissnan/hexo-theme-next) is the theme selected in [toub.es](https://toub.es), remember also to change properties like `google_analytics`, `social` in `themes/next/_config.yml`.

### Prerequisites

* Docker (https://www.docker.com/)

```
$ docker --version
Docker version 17.03.0-ce, build 3a232c8
```

* Docker Compose (https://docs.docker.com/compose/)

```
$> docker-compose --version
docker-compose version 1.11.2, build dfed245
```

### Install & Run

```
git clone https://gitlab.com/xcafebabe/xcafebabe.gitlab.io.git \
  && cd xcafebabe.gitlab.io \
  && cp env-example .env \
  && sudo docker-compose up
```

Once the site is up & running (wait until you see `Hexo is running at http://localhost:4000/. Press Ctrl+C to stop.` on the console), open a browser
and go to http://localhost:4000

## Development

All development tasks are recommended to be executed inside the container.

### To access hexo container

```
$> sudo docker exec -it toub-hexo /bin/sh
```

### To create a blog post

```
$> hexo new post "My Hello World!"
```

### To generate blog content

```
$> hexo generate
```


## Built With

* [Docker](https://www.docker.com/) - Container technology
* [Hexo](https://hexo.io/) - Static-site generator based on Node.js
* [hexo-theme-next](http://theme-next.iissnan.com/) - Elegant Theme for Hexo

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/xcafebabe/xcafebabe.gitlab.io/tags).

## Authors

* **Luis Toubes** - [xcafebabe](https://gitlab.com/xcafebabe)

## License

This project is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License - see the [LICENSE.md](LICENSE.md) file for details

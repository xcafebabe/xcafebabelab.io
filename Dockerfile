FROM node:8.12.0-alpine

LABEL maintainer="Luis Toubes <luis@toub.es>"

## Server Port
ARG HEXO_SERVER_PORT=4000
ENV HEXO_SERVER_PORT ${HEXO_SERVER_PORT}

## Root Folder
ARG HEXO_ROOT_PATH=/hexo
ENV HEXO_ROOT_PATH ${HEXO_ROOT_PATH}

## Server Options
ARG HEXO_SERVER_OPTIONS=-d
ENV HEXO_SERVER_OPTIONS ${HEXO_SERVER_OPTIONS}

## Install Hexo Cli Component
RUN npm config set unsafe-perm true \
  && npm install hexo-cli -g  --ignore-scripts \
  && chown -R root:root /usr/local/lib/node_modules/hexo-cli/node_modules \
  && chmod 755 -R /usr/local/lib/node_modules/hexo-cli/node_modules \
  && apk --update --no-cache add \
  bash \
  coreutils \
  automake \
  git \
  execline \
  alpine-sdk  \
  nasm  \
  autoconf  \
  build-base \
  zlib \
  zlib-dev \
  libpng \
  libpng-dev \
  libwebp \
  libwebp-dev \
  libjpeg-turbo \
  libjpeg-turbo-dev

## A base Hexo Blog
WORKDIR ${HEXO_ROOT_PATH}

VOLUME ["${HEXO_ROOT_PATH}"]

## Expose the HEXO_SERVER_PORT
EXPOSE ${HEXO_SERVER_PORT}

USER node

## Development Purposes
CMD npm install && hexo clean && hexo server ${HEXO_SERVER_OPTIONS} -p ${HEXO_SERVER_PORT}
